import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { width, height } from '../utils/dimensions';
import Color from '../utils/Color';

export default class Button extends React.Component {
  render(){
    return(
      <TouchableOpacity onPress={this.props.onPress} style={[{width: width*0.4, height: height*0.2, borderRadius: 9, backgroundColor: Color.primaryColor, justifyContent: 'center', alignItems: 'center'}, this.props.style]}>
        <Text style={[{color: Color.white}, this.props.styleText]}>{this.props.text}</Text>
      </TouchableOpacity>
    )
  }
}
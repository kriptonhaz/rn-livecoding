import * as parts from './parts';

export const DataInput = (passProps) => ({
  component: {
    id: 'DataInput',
    name: 'DataInput',
    passProps: passProps,
    options: {
      statusBar: parts.statusBar(),
      topBar: parts.topBar(),
      animations: {
        push: parts.animationPush(),
        pop: parts.animationPop()
      }
    }
  }
})

export const DataView = (passProps) => ({
  component: {
    id: 'DataView',
    name: 'DataView',
    passProps: passProps,
    options: {
      statusBar: parts.statusBar(),
      topBar: parts.topBar(),
      animations: {
        push: parts.animationPush(),
        pop: parts.animationPop()
      }
    }
  }
})

export const DataUpdate = (passProps) => ({
  component: {
    id: 'DataUpdate',
    name: 'DataUpdate',
    passProps: passProps,
    options: {
      statusBar: parts.statusBar(),
      topBar: parts.topBar(),
      animations: {
        push: parts.animationPush(),
        pop: parts.animationPop()
      }
    }
  }
})
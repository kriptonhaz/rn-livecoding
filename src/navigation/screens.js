import * as views from './view';
import * as parts from './parts';

export {
  views,
  parts
}
import { Navigation } from 'react-native-navigation';

import DataInput from '../screens/DataInputScreen';
import DataView from '../screens/DataViewScreen';
import DataUpdate from '../screens/DataUpdateScreen';

export function registerScreen(){
  Navigation.registerComponent('DataInput', ()=>DataInput);
  Navigation.registerComponent('DataView', ()=>DataView);
  Navigation.registerComponent('DataUpdate', ()=>DataUpdate);
}
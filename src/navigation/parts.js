export const animationPush = () => ({
  enabled: true,
  content: {
    x: {
      from: 1000,
      to: 0,
      duration: 300,
      interpolation: 'decelerate',
    }
  }
});

export const animationPop = () => ({
  enabled: true,
  // waitForRender: true,
  content: {
    x: {
      from: 0,
      to: 1000,
      duration: 300,
      interpolation: 'decelerate',
    }
  }
});

export const statusBar = () => ({
  backgroundColor: 'transparent',
  visible: true,
  style: 'dark',
  drawBehind: true
})

export const topBar = () => ({
  visible: false,
  drawBehind: true
})
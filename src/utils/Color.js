export const Color = {
  primaryColor: '#4242f4',
  secondaryColor: '#bf42f4',
  white: '#ffffff'
}

export default Color;
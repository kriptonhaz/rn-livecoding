import React from 'react';
import { View, Text, TextInput } from 'react-native';
import Button from '../component/button';
import API from '../utils/request';

export default class DataInput extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      name: '',
      job: ''
    }
  }

  onSubmit = () => {
    let payload = {
      "name" : this.state.name,
      "job" : this.state.job
    }
    API.post('/users/', payload).then((res)=>{
      console.log('res = '+JSON.stringify(res));
    })
  }

  render(){
    return(
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <TextInput 
          style={{width: 200, height: 50, borderColor: 'blue', borderWidth: 1}}
          onChangeText={(text) => this.setState({name: text})}
          placeholder={'Name'}
        />
        <TextInput 
          style={{width: 200, height: 50, borderColor: 'blue', borderWidth: 1}}
          onChangeText={(text) => this.setState({job: text})}
          placeholder={'Job'}
        />
        <Button 
          text={'SUBMIT'}
          onPress={()=>this.onSubmit()}
        />
      </View>
    )
  }
}
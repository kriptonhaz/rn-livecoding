import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { Navigation } from 'react-native-navigation';
import Button from '../component/button';
import navigation from '../navigation';
import axios from 'axios';
import {width, height} from '../utils/dimensions';
import API from '../utils/request';

export default class DataView extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      data : []
    }
  }

  componentDidMount(){
    API.get('/users/').then((response)=>{
      console.log('response = '+JSON.stringify(response.data))
      this.setState({data: response.data});
    })
  }
  
  gotoInput = () => {
    Navigation.push(this.props.componentId, navigation.views.DataInput());
  }

  gotoUpdate = () => {
    Navigation.push(this.props.componentId, navigation.views.DataUpdate());
  }

  render(){
    return(
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <FlatList 
          style={{marginTop: 50}}
          data={this.state.data}
          ItemSeparatorComponent={() => <View style={{height: 10, width: width}}/>}
          renderItem={({item}) =>
            <View style={{marginBottom: 10}}>
              <Text>{item.id}</Text>
              <Text>{item.email}</Text>
              <Text>{item.first_name}</Text>
            </View>
          }
          keyExtractor={(item) => item.id.toString()}
        />
        {/* <Text>SCREEN DATA VIEW</Text> */}
        {/* <Button 
          text={'KE SCREEN INPUT'}
          onPress={()=>this.gotoInput()}
        />
        <Button 
          text={'KE SCREEN UPDATE'}
          onPress={()=>this.gotoUpdate()}
        /> */}
        <TouchableOpacity onPress={()=>this.gotoInput()} style={{position: 'absolute', width: 50, height: 50, borderRadius: 25, backgroundColor: '#f47a42', right: 5, bottom: 20, justifyContent: 'center', alignItems: 'center'}}>
          <Text>ADD</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
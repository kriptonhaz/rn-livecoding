/**
 * @format
 */

// import {AppRegistry} from 'react-native';
// import App from './App';
// import {name as appName} from './app.json';

// AppRegistry.registerComponent(appName, () => App);

import { Navigation } from "react-native-navigation";
import { registerScreen } from './src/navigation/config';
import navigation from './src/navigation';

registerScreen();

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [navigation.views.DataView()],
        options: {
          statusBar: navigation.parts.statusBar(),
          topBar: navigation.parts.topBar(),
          animations: {
            push: navigation.parts.animationPush(),
            pop: navigation.parts.animationPop()
          }
        }
      }
    }
  });
});
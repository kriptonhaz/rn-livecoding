/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput} from 'react-native';
import Button from './src/component/button';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      sample: ''
    }
  }

  onchange = (param) => {
    this.setState({sample: param});
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput 
          style={{borderWidth: 1, borderColor: 'red'}}
          onChangeText={(text) => this.onchange(text)}
          placeholder={'Input your text here'}
        />
        <Icon name='rocket' size={30} color={'blue'}/>
        <Button 
          text={'PRESS HERE'}
          style={{width: 100, height: 30}}
          styleText={{color: 'black'}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
